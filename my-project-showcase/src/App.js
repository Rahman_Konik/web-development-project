import React from 'react';
import './App.css';

function App() {

  return (
    <div className="myBackground">

      <nav>
        <ul>
          <li>Home</li>
          <li>Video</li>
          <li>Project Technology</li>
          <li>Contact</li>
        </ul>
      </nav>

      <main>
        <h1>Welcome to 3D Printer Project</h1>
        <p>We implemented a feature which can help the user to added the multiple printer by FDM manster ( Printer manager)
          <br></br> and
          Using an application api key which took from Octprinter.
        </p>
        <button className="mybutton"> Read Me !</button>

        <div className='myImage'>

            <img src="compoments/diagram.png" alt="image"/>

        </div>

      </main>

      <footer>
        &copy; 2024 3D2.ilab.fi. All rights reserved by 3D-Group-2.
      </footer>
    </div>
  );
}

export default App;
