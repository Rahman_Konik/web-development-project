
let fullName = "Konik Rahman";
console.log(fullName);

let number = 6;
console.log(number);

const value = 12;
console.log(value);

// Object

const student = {
    fullName: "Onik",
    age: 25,
    isStudent: true,
    cgpa: 5.5,
};

console.log(student.fullName);
console.log(student["age"]);